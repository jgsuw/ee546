import numpy as np
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation

def anim(data,p,fign=-2,fps=24):

    (k,t,j,o) = data

    # def spring(z0,z1,a=.2,b=.6,c=.2,h=1.,p=4,N=100):
    #     x = np.linspace(0.,a+b+c,N); y = 0.*x
    #     mb = int(N*a/(a+b+c)); Mb = int(N*(a+b)/(a+b+c))
    #     y[mb:Mb] = np.mod(np.linspace(0.,p-.01,Mb-mb),1.)-0.5
    #     z = ((np.abs(z1-z0)*x + 1.j*h*y)) * np.exp(1.j*np.angle(z1-z0)) + z0
    #     return z

    # def damper(z0,z1,a1,a2,b1,b2,f):
    #     #assert 0. <= f <= 1.
    #     #assert 1. >= 1.-f >= 0.
    #     l = np.abs(z1-z0)
    #     x1 = np.asarray([0.,a1])
    #     y1 = np.asarray([0.,0.])
    #     x2 = np.asarray([a1+a2,a1,a1,a1+a2])
    #     y2 = np.asarray([+b1,+b1,-b1,-b1])
    #     x3 = np.asarray([a1+a2*(1.-f),
    #                     a1+a2*(1.-f),
    #                     a1+a2*(1.-f),
    #                     l])
    #     y3 = np.asarray([+b2,-b2,0.,0.])
    #     x = np.hstack((x1,np.nan,x2,np.nan,x3))
    #     y = np.hstack((y1,np.nan,y2,np.nan,y3))
    #     z = ((x + 1.j*y)) * np.exp(1.j*np.angle(z1-z0)) + z0
    #     return z

    def Ellipse(x,y, rx, ry, N=20, t=0, **kwargs):
        theta = 2*np.pi/(N-1)*np.arange(N)
        xs = x + rx*np.cos(theta)*np.cos(-t) - ry*np.sin(theta)*np.sin(-t)
        ys = y + rx*np.cos(theta)*np.sin(-t) + ry*np.sin(theta)*np.cos(-t)
        return xs, ys

    def bone(x,y,θ,L):
        r = .01
        xf = []
        yf = []
        xf += [x+r*np.cos(z+θ) for z in [3*np.pi/2, np.pi, np.pi/2]]
        yf += [y+r*np.sin(z+θ) for z in [3*np.pi/2, np.pi, np.pi/2]]
        xf += [x+r*np.cos(z+θ)+L*np.cos(θ) for z in [np.pi/2, 0, -np.pi/2]]
        yf += [y+r*np.sin(z+θ)+L*np.sin(θ) for z in [np.pi/2, 0, -np.pi/2]]
        return xf, yf

    r = 0.025

    dd = 5*r

    lw = 1
    mew = lw
    ms = 10
    lt = '-'
    fs = (24,24)

    col1 = '#1f77b4' # blue
    col2 = '#ff7f0e' # orange

    fig = plt.figure(fign,figsize=fs)
    plt.clf()
    ax = fig.add_subplot(111,aspect='equal')

    body, = ax.fill([0.],[0.],fc='#ff7f0e',ec='k',lw=lw,animated=True)
    l_femur, = ax.fill([0.],[0.],fc='#1f77b4',ec='k',lw=lw,animated=True)
    r_femur, = ax.fill([0.],[0.],fc='#1f77b4',ec='k',lw=lw,animated=True)
    l_tibia, = ax.fill([0.],[0.],fc='#1f77b4',ec='k',lw=lw,animated=True)
    r_tibia, = ax.fill([0.],[0.],fc='#1f77b4',ec='k',lw=lw,animated=True)
    foot, = ax.fill([0.],[0.],fc='#1f77b4',ec='k',lw=lw,animated=True)

    xdata = []
    zdata = []

    def init():

        ax.grid('on')

        ax.set_xlim(-.25,.25)
        ax.set_ylim(-.25,1)

        ax.set_ylabel(r'height')
        # return list(bodies.values()) + [leg,hip,foot]
        return [body,foot,l_femur,r_femur,l_tibia,r_tibia]

    def animate(_t):
        i = (t >= _t).nonzero()[0][0]
        # animate body
        x = 0
        y = o['q'][i,2]
        x,y = Ellipse(x,y,r,r,N=20)
        body.set_xy(np.array([x,y]).T)

        # animate foot
        x = o['q'][i,0]
        y = o['q'][i,1]
        x,y = Ellipse(x,y,r/2,r/2,N=20)
        foot.set_xy(np.asarray([x,y]).T)

        # foo
        x,y = bone(0,o['q'][i,2],-o['q'][i,3]+o['q'][i,4]-np.pi/2,p['a'])
        l_femur.set_xy(np.asarray([x,y]).T)
        x,y = bone(0,o['q'][i,2],o['q'][i,3]+o['q'][i,4]-np.pi/2,p['a'])
        r_femur.set_xy(np.asarray([x,y]).T)

        z = np.arcsin(p['a']*np.sin(o['q'][i,3])/p['b'])
        x,y = bone(o['q'][i,0],o['q'][i,1],o['q'][i,4]+np.pi/2-z,p['b'])
        l_tibia.set_xy(np.asarray([x,y]).T)
        x,y = bone(o['q'][i,0],o['q'][i,1],o['q'][i,4]+np.pi/2+z,p['b'])
        r_tibia.set_xy(np.asarray([x,y]).T)

        return [body,foot,l_femur,r_femur,l_tibia,r_tibia]
            # return list(bodies.values()) + [leg,hip,foot]

    ani = FuncAnimation(fig, animate, np.arange(0.,t[-1],t[-1]*10*1e-3), init_func=init,
                        blit=True, repeat=True, interval=10)

    # plt.ion()
    plt.show()
    return ani