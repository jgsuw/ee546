#%%
import numpy as np
from multiprocessing import Pool
# Brute-force search of gridded space. There are 4 decision variables:
# 1) touchdown angle 2) touchdown body velocity 3) extension torque 4) hip torque


#%% Search range
θ_td = np.pi/180*np.linspace(50,85,36)
dyb_td = np.linspace(0,1,11)
extension_torque = np.linspace(5,20,16)
rotation_torque = np.linspace(0,10,11)

def cartesian_product_set(A,B):
    product = ()
    for i in range(len(A)):
        for j in range(len(B)):
            a = A[i]
            if type(a) != tuple:
                a = (a,)
            b = B[j]
            if type(b) != tuple:
                b = (b,)
            element = ()
            element += a
            element += b
            product += (element,)
    return product

f = lambda A,B: cartesian_product_set(A,B)
search_params = f(θ_td,f(dyb_td,f(extension_torque,rotation_torque))) 
# total number of search parameters: 69,696

# construct process args
directory = 'simdata/'
filenames = [directory+'sim%d' % i for i in range(3)]
R = np.zeros((4,4))
R[:,:2] = 1e-2*np.eye(4)[:,:2]
process_args = [(filenames[i],search_params[i],R) for i in range(3)]

# run the pool
if __name__ == "__main__":
    p = Pool(3)
    from grid_search_process import task
    p.map(task,process_args)