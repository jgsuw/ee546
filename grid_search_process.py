from model import Model, calc_leg_int_angle, calc_leg_len
from heuristic_controller import Controller
import numpy as np
import pickle
import os
import errno

def task(args):
    output_path = args[0]
    controller_params = args[1]
    R = args[2]

    # Prepare output data location
    directory = os.path.dirname(output_path)
    if not os.path.exists(directory):
        try:
            os.makedirs(directory)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    outfile = open(output_path,'wb')
    
    # Get controller and model data
    model_params = {
        'a' : .1,   # femur length
        'b' : .2,   # tibia length
        'd' : .2,   # distance to the wall
        'm' : 2,    # mass
        'mu': .9,   # coefficient of friciton
        'g' : 10
    }

    # Construct model
    model = Model(model_params, reproject=False)

    # Construct controller  
    init_state = "pushoff"
    θ_td = controller_params[0]
    l_td = model_params['d']/np.sin(θ_td)
    ϕ_td = calc_leg_int_angle(model_params['a'], model_params['b'],l_td)
    dyb_td = controller_params[1]
    rt_setpoint = [None,None,None,np.pi/2,None, None,None,None,0,0]
    td_setpoint = [None,None,None,ϕ_td,θ_td,None,None,dyb_td,0,0]
    extension_torque = controller_params[2]
    rotation_torque = controller_params[3]

    controller = Controller(init_state, 
                            td_setpoint, 
                            rt_setpoint,
                            extension_torque,
                            rotation_torque,
                            model_params)

    #%% Set up initial state
    x0 = np.zeros(model.len_x)
    j0 = 1
    x0[0] = model_params['d']   # xfoot
    x0[1] = -l_td*np.cos(θ_td)  # yfoot
    x0[2] = 0                   # ybody
    x0[3] = ϕ_td                # ϕ
    x0[4] = θ_td                # θ
    x0[5] = 0                   # dxfoot
    x0[6] = 0                   # dyfoot
    x0[7] = dyb_td              # dybody
    x0[8] = 0                   # dϕ
    x0[9] = 0                   # dθ

    #%% Simulation parameters
    tf = 2
    dt = 1e-3
    K = 100
    p = dict(controller=controller)
    rx = 1e-12

    # Do simulation
    trjs = model.sim_rx(K,tf,j0,x0,p,dt,rx)
    (K,T,J,O) = model.obs(trjs,None)

    # Calculate cost
    cost = -O['q'][-1,2]
    N = len(T)
    for i in range(N):
        u = O['u'][i,:]
        cost += (u@R@u)/N

    # Package results
    data = {
        'controller_params': controller_params,
        'tf' : tf,
        'dt' : dt,
        'rx' : rx,
        'trjs': trjs,
        'cost': cost
    }
    pickle.dump(data, outfile)
    outfile.close()
    return

if __name__ == "__main__":

    output_path = 'simdata/foo'

    θ_td = 55*np.pi/180
    yb_td = 0
    extension_torque = 10
    rotation_torque = 0

    R = np.zeros((4,4))
    R[0,0] = 1e-2
    R[1,1] = 1e-2

    task((output_path,(θ_td,yb_td,extension_torque,rotation_torque),R))

    


