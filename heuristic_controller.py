class Controller:
    def __init__(self, init_state, 
                td_setpoint, 
                rt_setpoint, 
                extension_torque, 
                rotation_torque,
                model_params):

        self.state = init_state
        self.setpoint = None
        self.td_setpoint = td_setpoint
        self.rt_setpoint = rt_setpoint
        self.time_constant = None
        self.damping_ratio = .9
        self.model_params = model_params

    def update(self,k,t,j,x):
        if j == 0:
            if self.state == "retract" and x[3] > self.rt_setpoint[3]:
                self.state = "touchdown"
                self.time_constant = abs(x[7]-self.td_setpoint[7])/self.model_params['g']/10
            elif self.state == "pushoff":
                self.state = "retract"
                self.time_constant = abs(x[7]-self.td_setpoint[7])/self.model_params['g']/20
        if j == 1:
            self.state = "pushoff"
    def output(self,k,t,j,x):
        if j == 0:
            if self.state == "retract":
                K1 = 1./self.time_constant**2
                K2 = 2*self.damping_ratio*self.time_constant*K1
                K3 = 1./self.time_constant
                u3 = K1*(self.rt_setpoint[3]-x[3])+K2*(self.rt_setpoint[8]-x[8])
                u4 = K3*(self.rt_setpoint[9]-x[9])
                return [0,0,u3,u4]
            elif self.state == "touchdown":
                K1 = 1./self.time_constant**2
                K2 = 2*self.damping_ratio*self.time_constant*K1
                K3 = 1./self.time_constant**2
                K4 = 2*self.damping_ratio*self.time_constant*K3
                u3 = K1*(self.td_setpoint[3]-x[3])+K2*(self.td_setpoint[8]-x[8])
                u4 = K3*(self.td_setpoint[4]-x[4])+K4*(self.td_setpoint[9]-x[9])
                return [0,0,u3,u4]
        elif j == 1:
            u1 = -10
            u2 = 0
            return [u1,u2,0,0]
        else:
            raise BaseException("Invalid discrete state")
