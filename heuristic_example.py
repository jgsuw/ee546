#%%
from model import Model, calc_leg_len, calc_leg_int_angle
from heuristic_controller import Controller
import numpy as np
from matplotlib import pyplot as plt

params = {
    'a' : .1,   # femur length
    'b' : .2,   # tibia length
    'd' : .2,   # distance to the wall
    'm' : 2,    # mass
    'mu': .9,   # coefficient of friciton
    'g' : 10
}
#%% Construct model
model = Model(params, reproject=False)

# Set up controller
init_state = "pushoff"
θ_td = 85*np.pi/180
l_td = params['d']/np.sin(θ_td)
ϕ_td = calc_leg_int_angle(params['a'],params['b'],l_td)
yb_td = 0
rt_setpoint = [None,None,None,np.pi/2,None, None,None,None,0,0]
td_setpoint = [None,None,None,ϕ_td,θ_td,None,None,yb_td,0,0]
controller = Controller(init_state, td_setpoint, rt_setpoint, params)

#%% Set up initial state
x0 = np.zeros(model.len_x)
j0 = 1
x0[0] = params['d']         # xfoot
x0[1] = -l_td*np.cos(θ_td)  # yfoot
x0[2] = 0                   # ybody
x0[3] = ϕ_td                # ϕ
x0[4] = θ_td                # θ
x0[5] = 0                   # dxfoot
x0[6] = 0                   # dyfoot
x0[7] = 0                   # dybody
x0[8] = 0                   # dϕ
x0[9] = 0                   # dθ

#%% Simulation parameters
tf = 1
dt = 1e-3
K = 100
p = dict(controller=controller)
rx = 1e-12

# Do simulation
trjs = model.sim_rx(K,tf,j0,x0,p,dt,rx)
(K,T,J,O) = model.obs(trjs,p)

#%% Make figure
fig1, axs1 = plt.subplots(nrows=3,ncols=1)
axs1[0].plot(T,O['q'][:,2])
axs1[0].set_xlabel('time')
axs1[0].set_ylabel('Body height')

axs1[1].plot(T,O['l'])
axs1[1].set_xlabel('time')
axs1[1].set_ylabel('Leg length (m)')

axs1[2].plot(T,180/np.pi*O['q'][:,4])
axs1[2].set_xlabel('time')
axs1[2].set_ylabel('leg angle (degrees)')
# fig.show()

fig2, axs2 = plt.subplots(nrows=2,ncols=1)
axs2[0].plot(T,O['λ'][:,1])
axs2[0].set_xlabel('time')
axs2[0].set_ylabel('Normal Force (N)')

axs2[1].plot(T,O['λ'][:,0])
axs2[1].set_xlabel('time')
axs2[1].set_ylabel('Tangential Force (N)')
plt.show()

#%% Make an animation of the results
# from animation import anim
# import matplotlib.animation as mplanimation

# Writer = mplanimation.writers['ffmpeg']
# writer = Writer(fps=30,bitrate=1800)
# foo = anim((K,T,J,O), params, fps=30)
# foo.save('climb.mp4',writer=writer)