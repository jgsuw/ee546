#%%
import numpy as np 
import sympy as sp
from scipy.optimize import fsolve
from sympy.physics.vector import dynamicsymbols
from hybrid_systems import Hybrid

# Model constants
(_a,_b,_d,_μ,_m,_g) = sp.symbols('a b d μ m g')

# Model control inputs
(_u1,_u2,_u3,_u4) = sp.symbols('u1 u2 u3 u4')
_u = (_u1,_u2,_u3,_u4) 

# Model position and velocity symbols
_t = sp.symbols('t')
(_xf, _yf, _yb, _ϕ, _θ) = dynamicsymbols('xf yf yb ϕ θ')
_coord_syms = (_xf, _yf, _yb, _ϕ, _θ)
_n = len(_coord_syms)
(_dxf, _dyf, _dyb, _dϕ, _dθ) = tuple([sp.diff(sym, (_t,1)) for sym in _coord_syms])
_vel_syms = (_dxf, _dyf, _dyb, _dϕ, _dθ)

# Leg length map
_l = _a*sp.cos(_ϕ)+(_b**2-(_a*sp.sin(_ϕ))**2)**.5

# Array of constraints taking the form a(x) = 0
_A = np.array([
    _yb-_yf-_l*sp.cos(_θ),
    _xf-_l*sp.sin(_θ),
], dtype=object)


# Constraints taking the form b(x)dx = 0
_B = np.array([
    [1,0,0,0,0],
    [0,1,0,0,0]
], dtype='object')

# Array of active constraints from A(x) per contact mode
_IAc = np.array([
    [0,1],      # Flight
    [0,1]       # Stick
])

_IBc = np.array([
    [],
    [0,1]
])

# Array of independent coordinates per contact mode
_Ixi = np.array([
    [2,3,4],
    [0,1,2]
])

# Array of dependent coordinates per contact mode
_Ixd = np.array([
    [0,1],
    [3,4]
])

# Array of independent velocities per contact mode
_Idxi = np.array([
    [2,3,4],
    [2]
])

# Array of dependent velocities per contact mode
_Idxd = np.array([
    [0,1],
    [0,1,3,4]
])

# Array of gradient vectors for each constraint function
# _DA = np.array([[sp.diff(f,(var,1)) for var in _coord_syms] for f in _A],dtype=object)
_DA = np.array([
    [[sp.diff(f,(var,1)) for var in _coord_syms] for f in _A[_IAc[0]]],
    [[sp.diff(f,(var,1)) for var in _coord_syms] for f in _A[_IAc[1]]]
], dtype=object)

# Total derivative of each element in _DA w.r.t. time
# _DA_p = np.array([[sp.diff(f,(_t,1)) for f in row] for row in _DA],dtype=object)
_DA_p = np.array([
    [[sp.diff(f,(_t,1)) for f in row] for row in _DA[0]],
    [[sp.diff(f,(_t,1)) for f in row] for row in _DA[1]]
], dtype=object)

# Total derivative of each element in _B w.r.t. time
# _B_p = np.array([[sp.diff(f,(_t,1)) for f in row] for row in _B],dtype=object)
_B_p = np.array([
    [[sp.diff(f,(_t,1)) for f in row] for row in _B[_IBc[0]]],
    [[sp.diff(f,(_t,1)) for f in row] for row in _B[_IBc[1]]]
], dtype=object)

# Jacobian of constraints per contact mode
_J = np.array([
    np.vstack((_DA[0],_B[_IBc[0]])),
    np.vstack((_DA[1],_B[_IBc[1]]))
], dtype=object)

# Derivative of jacobian of all constraints
_Jp = np.array([
    np.vstack((_DA_p[0],np.reshape(_B_p[0],(len(_IBc[0]),_n)))),
    np.vstack((_DA_p[1],np.reshape(_B_p[1],(len(_IBc[1]),_n))))
], dtype=object)

# Array of permutation matrices per contact mode
# used to partition states into independent and dependent coordinates
_Px = np.array([
    np.vstack((np.vstack(np.eye(_n)[_Ixi[0],:]),np.vstack(np.eye(_n)[_Ixd[0],:]))),
    np.vstack((np.vstack(np.eye(_n)[_Ixi[1],:]),np.vstack(np.eye(_n)[_Ixd[1],:])))
])

_Pdx = np.array([
    np.vstack((np.vstack(np.eye(_n)[_Idxi[0],:]),np.vstack(np.eye(_n)[_Idxd[0],:]))),
    np.vstack((np.vstack(np.eye(_n)[_Idxi[1],:]),np.vstack(np.eye(_n)[_Idxd[1],:])))
])
# Array of Lagrangians
_L = np.array([
    .5*_m*_dyb**2-_m*_g*_yb+.5*_dϕ**2+.5*_dθ**2,
    .5*_m*_dyb**2-_m*_g*_yb
], dtype=object)

# Array of Lagrange multipliers associated with constraints A(x)
_λA = np.array(
    [sp.symbols('λA%d'%(i,)) for i in range(len(_A))],
 dtype=object)

# Array of Lagrange multipliers associated with constraints B(x)dx
_λB = np.array(
    [sp.symbols('λB%d'%(i,)) for i in range(len(_B))]
)

# Array of all Lagrange multipliers in each contact mode
_λ = np.array([
    np.hstack((_λA[_IAc[0]],_λB[_IBc[0]])),
    np.hstack((_λA[_IAc[1]],_λB[_IBc[1]]))
 ], dtype=object)

# Array of external + nonconservative forces on system per contact mode
_f = np.array([
    [0,0,0,_u3,_u4],
    [0,0,0,_u1,_u2]
], dtype=object)

def _make_euler_lagrange(i,j):
# calculates euler lagrange equation for ith contact mode and jth coordinate
    # Collect problem data for constraint mode i
    L = _L[i]
    DA = _DA[0]
    B = _B[_IBc[i]]
    λA = _λA[_IAc[i]]
    n = len(λA)
    λB = _λB[_IBc[i]]
    m = len(λB)
    # Lagrangian terms
    eqn = sp.diff(L,_coord_syms[j]) - sp.diff(sp.diff(L,_vel_syms[j]),(_t,1))
    # External forces
    eqn += _f[i,j]
    # Constraint forces
    if n > 0:
        eqn += np.sum(λA*DA[:,j])
    if m > 0:
        eqn += np.sum(λB*B[:,j])
    return sp.simplify(eqn)


# Array of euler-lagrange systems per contact mode
_EL = np.array([
    [_make_euler_lagrange(0,j) for j in range(len(_coord_syms))],
    [_make_euler_lagrange(1,j) for j in range(len(_coord_syms))]
], dtype=object)

#%%
# Array of solutions for Lagrange multipliers per contact mode
# from lagrange equations of dependent coordinates
_Λ = np.array([
    sp.solve(_EL[0,_Idxd[0]],_λ[0].tolist()),
    sp.solve(_EL[1,_Idxd[1]],_λ[1].tolist())
], dtype=object)

# Solve equations of motion of independent velocities
_F = np.array([
    [sp.solve(_EL[0,i], sp.diff(_vel_syms[i],_t))[0] for i in _Idxi[0]],    # could be multiple bloody solutions
    [sp.solve(_EL[1,i], sp.diff(_vel_syms[i],_t))[0] for i in _Idxi[1]],    # going to only take the first one
], dtype=object)
# WARNING: calls to solve might return multiple solutions. Here I am ASSUMING
# that there is only one.

# Guard conditions per contact mode
_G = np.array([
    [_d-_xf],                            # non-penetration guard
    [-_λB[0], -_μ*_λB[0]-sp.Abs(_λB[1])]  # positive normal force and no-slip
], dtype=object)

# Not sure about reset maps for now.
# I believe the solution should be dyb' = dyb and then
# can solve for dϕ', dθ' as a linear function of (Da)dx=0 (constraint velocity is zero in contact)

# For now, I have most of the data necessary to solve the problem, so I will start to incorporate it into the model class

#%%
class Model(Hybrid):

    def __init__(self, params, reproject=False):
        Hybrid.__init__(self)
        self.params = params
        self.reproject = reproject

        # Indexes of data in the variable x
        self.nq = _n
        self.nu = len(_u)
        self.nλ = len(_λA)+len(_λB)
        self.len_x = self.nq+self.nq+self.nu+self.nλ

        self.q_idx = 0
        self.dq_idx = self.q_idx+self.nq
        self.u_idx = self.dq_idx+self.nq
        self.λ_idx = self.u_idx+self.nu

        # Data from last call to self.F(...)
        self.q = []
        self.dq = []
        self.u = []
        self.λ = []
        self.g = []
        self.J = []
        self.Jp = []

        # params: contains model parameters (a,b,d,μ,m,g) 
        self.subvals = {
            _a: params['a'],
            _b: params['b'],
            _d: params['d'],
            _μ: params['mu'],
            _m: params['m'],
            _g: params['g'],
        }

        # do substitutions for all problem data and lambdify results
        self._A = [sp.lambdify(
            [_coord_syms], 
            sp.Matrix(row.tolist()).subs(self.subvals),
            'numpy')
            for row in [_A[_IAc[i]] for i in range(len(_IAc))]
        ]
        self._B = [sp.lambdify(
            [_coord_syms], 
            sp.Matrix(row.tolist()).subs(self.subvals),
            'numpy')
            for row in [_B[_IBc[i]] for i in range(len(_IBc))]
        ]
        self._J = [sp.lambdify(
            [_coord_syms], 
            sp.Matrix(row.tolist()).subs(self.subvals),
            'numpy')
            for row in _J
        ]
        self._Jp = [sp.lambdify(
            [_coord_syms, _vel_syms], 
            sp.Matrix(row.tolist()).subs(self.subvals),
            'numpy')
            for row in _Jp
        ]
        self._F = [sp.lambdify(
            [_coord_syms, _vel_syms, _λ[i], _u],
            sp.Matrix(_F[i]).subs(self.subvals),
            'numpy')
            for i in range(len(_F))
        ]
        self._λ = [
            sp.lambdify(
                [_coord_syms, _vel_syms, _u],
                sp.Matrix(list(λ.values())).subs(self.subvals),
                'numpy'
            )
            for λ in _Λ  
        ]
        self._G = [
            sp.lambdify(
                [_coord_syms, _vel_syms, _u, _λ[j]],
                sp.Matrix([f.subs(self.subvals) for f in _G[j]]),
                'numpy'
            )
            for j in range(len(_G))
        ]

        self._data = {}

    def AProjection(self,j,known,unknown,q):
        nq = _n
        ncx = len(_IAc[j])
        P1 = np.eye(nq)[known,:]
        P2 = np.eye(nq)[unknown,:]
        A = self._A[j]
        J = self._J[j]
        f = lambda x: (A(P1.T@q[known]+P2.T@x)).flatten()
        df = lambda x: (J(P1.T@q[known]+P2.T@x)@P2.T)[:ncx]
        soln = fsolve(f,P2@q,fprime=df)
        q[unknown] = soln
        return q

    def F(self,k,t,j,x,p):
        dx = np.zeros(self.nq+self.nq+self.nu+self.nλ)

        # Project positions to constraint surface
        q = x[self.q_idx:self.dq_idx]
        if self.reproject:
            q = self.AProjection(j,_Ixi[j],_Ixd[j],q)
        self.q = q
        x[self.q_idx:self.dq_idx] = q

        # Project velocities to constraint surface
        J = self._J[j](q)
        self.J = J

        dq = x[self.dq_idx:self.u_idx]
        _dq = dq[_Idxi[j]]
        dq_ = np.linalg.solve(J[:,_Idxd[j]], -J[:,_Idxi[j]]@_dq)
        dq[_Idxd[j]] = dq_
        self.dq = dq
        x[self.dq_idx:self.u_idx] = dq
    
        # Query controller
        u = np.zeros((self.nu,1))
        if 'controller' in p:
            p['controller'].update(k,t,j,x)
            u = p['controller'].output(k,t,j,x)
        x[self.u_idx:self.λ_idx] = u
        self.u = u

        # Calculate lagrange multipliers
        λ = self._λ[j](q,dq,u)
        x[self.λ_idx:self.λ_idx+len(λ)] = λ.flatten()
        self.λ = λ

        # Calculate equations of motion
        d2q = np.zeros((self.nq,))
        _d2q = self._F[j](q,dq,λ,u).flatten()
        d2q[_Idxi[j]] = _d2q

        Jp = self._Jp[j](q,dq)
        self.Jp = Jp

        d2q_ = np.linalg.solve(J[:,_Idxd[j]],-Jp@dq-J[:,_Idxi[j]]@_d2q)
        d2q[_Idxd[j]] = d2q_

        # Calculate guard
        self.g = self._G[j](q,dq,u,λ).flatten()

        # Put everything together 
        dx[self.q_idx:self.dq_idx] = dq
        dx[self.dq_idx:self.u_idx] = d2q
        return dx

    def G(self,k,t,j,x,p):
        return self.g

    def R(self,k,t,j,x,p):        # This is hand coded, not systemetized yet
        g = self.g
        if j == 0: # in flight, no reset needed
            if g[0]<0:
                j = 1
        elif j == 1: # should also not need a reset, will be handled by velocity constraint projection
            if np.any(g<0):
                j = 0
        else:
            raise BaseException("Invalid discrete state.")
        return (k+1,t,j,x)

    def O(self,k,t,j,x,p,**args):
        o = dict()
        o['q'] = x[self.q_idx:self.dq_idx]
        o['dq'] = x[self.dq_idx:self.u_idx]
        o['u'] = x[self.u_idx:self.λ_idx]
        o['λ'] = x[self.λ_idx:]
        o['l'] = calc_leg_len(self.params['a'], self.params['b'], x[4])
        return o

# Utility functions
_leg_len_calc = sp.lambdify([_a,_b,_ϕ], _l, 'numpy')
def calc_leg_len(a,b,ϕ):
    return _leg_len_calc(a,b,ϕ)

_l_diff = sp.lambdify([_a,_b,_ϕ], sp.diff(_l,_ϕ), 'numpy')
def calc_leg_int_angle(a,b,l):
    ϕ0 = np.pi/4
    f = lambda x: _leg_len_calc(a,b,x)-l
    fprime = lambda x: _l_diff(a,b,x)
    ϕ = fsolve(f, ϕ0, fprime=fprime)[0]
    return ϕ

#%% Testing the model, starting in stance, with constant pushoff torque,
if __name__ == "__main__":
    # integrating until slip occurs
    import time
    # set model parameters
    params = {
        'a' : .1,
        'b' : .2,
        'd' : .2,
        'm' : 2,
        'mu': .9,
        'g' : 10
    }

    # controller
    class Controller:
        def __init__(self):
            pass
        def update(self,k,t,j,x):
            pass
        def output(self,k,t,j,x):
            if j == 0:
                return [0,0,0,0]
            if j == 1:
                return [6,0,0,0]
            else:
                raise BaseException("Invalid discrete state")

    foo = Model(params,controller=Controller(),reproject=False)

    # Set up initial state
    xfoot = params['d']
    theta = np.pi/2 - 35*np.pi/180
    ybody = 0
    x0 = np.array([xfoot,0,ybody,np.pi/4,theta,0,0,0,0,0])
    x0[:_n] = foo.AProjection(0,[0,2,4],[1,3],x0[:_n])
    print(x0)

    # Set up simulation
    tf = 1e-1
    dt = 1e-4
    N = int(tf/dt)
    x = x0
    idx = 2
    t0 = time.clock()
    for i in range(N):
        dx = foo.F(0,0,1,x)
        bar = foo.G(0,0,1,x)
        if np.any(bar<0):
            print('reset')
            break
        x = x + dt*dx
    print(x)
    print(1./np.tan(x[4]))
    t1 = time.clock()