#%%
import pickle
import numpy as np
from matplotlib import pyplot as plt

data = {}
with open('simdata/foo', 'rb') as f:
    data = pickle.load(f)

controller_params = data['controller_params']
tf = data['tf']
dt = data['dt']
rx = data['rx']
cost = data['cost']
trjs = data['trjs']

# %%
